# Analysing the compendiums of knowledge about IA on Wikipedia

[Documentation en français](README.md)

This repository contains the data and programs used by Aymeric Bouchereau and Antonin Segault for the analysis of the references included in IA-related Wikipedia articles. This work was presented in Valenciennes (France) during the H2PTM 2023 conference. The proceedings are published by ISTE :

Bouchereau, A. & Segault, A. (2023). Une synthèse raisonnée de l’IA ? Usage des références bibliographiques des articles de Wikipédia sur l’Intelligence artificielle. *H2PTM 2023, La fabrique du sens à l’ère de l’information numérique : enjeux et défis*. ISTE Éditions.

This programs and data are shared under the terms of the GNU General Public License version 3 : https://www.gnu.org/licenses/

## Corpus of bibliographic references

The analysis covers the six following articles, as they were displayed in december 2022 :

- [Intelligences artificielle (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Intelligence_artificielle)
- [Apprentissage automatique (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Apprentissage_automatique)
- [Apprentissage profond (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Apprentissage_profond)
- [Artificial intelligence (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Artificial_intelligence)
- [Machine Learning (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Machine_learning)
- [Deep learning (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Deep_learning)

All the bibliographic references used in these articles were manually extracted for the "References" and "Bibliography" sections. Semi-automated processes were then developped to extract from each reference (or retrieve, in the case of incomplete references) several metadata : publication date, language, title ... The references were also manually classified according to their source :

- science : articles from peer-reviewed journals, proceedings, chapters of books from scientific publishers ;
- media (*presse*) : articles from newspapers or recognized online media :
- books (*livre*) : monographies, textbooks ;
- public institutions (*institution publique*) : documents published by governement agencies, national agencies or supranational agencies (UN, EU) ;
- others (*autres*) : all the other references (including a large variety of websites).

This annotated corpus is shown in  **1\_all\_titles\_with_metadata.tsv**

## References distributions

In a first step, we examined the distribution of these references, regarding their types (source classification), languages and publication dates. The Python3 program **2\_stats.py** compares this data, producing the  **3\_ref\_stats.tsv** table. Part of this data was then presented in figure **4\_ref\_stats\_figure1.png** :

![4\_ref\_stats\_figure1.png](4_ref_stats_figure1.png)

## Title vocabulary

In a second step, we performed a textometric analysis of the references' titles. The program  **5\_title\_to\_stemms.py** uses the NLTP library (with the tools corresponding to the languages of each titles) to remove punctuation and stop words, and then to stemm the remaining words. The corpus, enriched with this data, is re-exported in  **6\_all\_titles\_stemmed\_with\_metadata.tsv**.

Another program, **7\_stemms\_to\_clouds.py**, generates tag clouds presented in the **8\_clouds** folder. Many clouds are produced, based on different data : grouping by reference type (ex : cloud\_type\_presse.png), by decade of publication (ex : cloud\_period\_2000.png), by article topic (ex : cloud\_topic\_IA.png) ... and a few crossing several variables (ex : cloud\_titles\_AI\_en\_science.png). For each cloud, the program also exports the top 20 most frequent stemms (top).

Among these clouds, the figure **9\_cloud\_topic\_figure.png** was used to show the differences in the vocabulary of the references used in the articles related to artificial intelligence (left), machine learning (center) and deep learning (right)) :

![9\_cloud\_topic\_figure.png](9_cloud_topic_figure.png)

Figure **10\_cloud\_type\_figure.png** is used to show the difference in vocabulary used by scientific sources (left) and press ones (right) :

![10\_cloud\_type\_figure.png](10_cloud_type_figure.png)


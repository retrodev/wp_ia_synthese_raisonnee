# Analyser les synthèses raisonnées de l'IA sur Wikipédia

[Documentation in english](README_en.md)

Ce dépôt rassemble les données et programmes utilisés par Aymeric Bouchereau et Antonin Segault pour leur analyse des références mobilisées dans les articles relatifs à l'IA. Ces travaux ont été présentés à Valenciennes, lors du colloque H2PTM 2023, dont les actes sont publiés par ISTE Éditions :

Bouchereau, A. & Segault, A. (2023). Une synthèse raisonnée de l’IA ? Usage des références bibliographiques des articles de Wikipédia sur l’Intelligence artificielle. *H2PTM 2023, La fabrique du sens à l’ère de l’information numérique : enjeux et défis*. ISTE Éditions.

Ces programmes et données sont partagés selon les termes de la licence GNU General Public License version 3 : https://www.gnu.org/licenses/

## Corpus des références bibliographiques

L'analyse porte sur les six articles suivants, tels qu'ils étaient visibles en décembre 2022 :

- [Intelligences artificielle (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Intelligence_artificielle)
- [Apprentissage automatique (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Apprentissage_automatique)
- [Apprentissage profond (sur l'encyclopédie francophone)](https://fr.wikipedia.org/wiki/Apprentissage_profond)
- [Artificial intelligence (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Artificial_intelligence)
- [Machine Learning (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Machine_learning)
- [Deep learning (sur l'encyclopédie anglophone)](https://en.wikipedia.org/wiki/Deep_learning)

Les références bibliographiques mobilisées dans ces articles ont été manuellement extraites de leurs sections Références et Bibliographie. Des procédures semi-automatiques ont été employées pour extraire de chaque référence (ou retrouver, dans le cas des références incomplètes) divers métadonnées : date de publication, langue, titre ... Les références ont également été manuellement classées selon le type de source dont elles proviennent :

- science : articles de revues à comité de lecture, actes de conférences, chapitres
d’ouvrages collectifs publiés auprès d’éditeurs scientifiques ;
- presse : articles de journaux ou de médias en ligne reconnus ;
- livre : monographies, manuels scolaires ;
- institution publique : documents produits par des agences gouvernementales, des
institutions publiques nationales ou supranationales (ONU, UE) ;
- autres : toutes les autres références (notamment un grand nombre de sites web).

Ce corpus annoté est présenté dans le tableau **1\_all\_titles\_with_metadata.tsv**

## Distribution des références

Dans une première étape d'analyse, la distribution de ces références a été examinée, notamment sur la base de leur type (cf supra), de leur langue et de leur date de parution. Le programme Python3 **2\_stats.py** croise ces données, produisant le tableau **3\_ref\_stats.tsv**. Une partie des ces données ont ensuite été présentées sous la forme de la figure **4\_ref\_stats\_figure1.png** :

![4\_ref\_stats\_figure1.png](4_ref_stats_figure1.png)

## Vocabulaire des titres

Dans une seconde étape, les titres des références ont fait l'objet d'une analyse textométrique. Le programme **5\_title\_to\_stemms.py** utile les outils de la librairie NLTK (dans la langue qui correspond à celle du titre) pour retirer la ponctuation et les mots vides des titres, puis raciniser les mots résultants. Le corpus, enrichi de ces données, est réexporté dans le tableau **6\_all\_titles\_stemmed\_with\_metadata.tsv**.

Un autre programme, **7\_stemms\_to\_clouds.py**, génère ensuite des nuages de mots clefs qui sont présentés dans le dossier **8\_clouds**. Plusieurs nuages sont générés sur la base de différents indicateurs : regroupement par type de référence (ex : cloud\_type\_presse.png), par décennie de référence (ex : cloud\_period\_2000.png), par sujet de l'article (ex : cloud\_topic\_IA.png) ... ainsi que pour quelques croisements de plusieurs de ces variables (ex : cloud\_titles\_AI\_en\_science.png). Pour chaque nuage, le programme exporte également une liste des 20 termes racinisés les plus fréquents (top).

Parmi les nuages de mots clefs, la figure **9\_cloud\_topic\_figure.png** a été employée pour montrer la différence de vocabulaire dans les références relatives à l'intelligence artificielle (à gauche), à l'apprentissage machine (au centre) et à l'apprentissage profond (à droite) :

![9\_cloud\_topic\_figure.png](9_cloud_topic_figure.png)

La figure **10\_cloud\_type\_figure.png** a, quant à elle, été employée pour illustrer la différence de vocabulaire dans les sources scientifiques (à gauche) et de presse (à droite ) :

![10\_cloud\_type\_figure.png](10_cloud_type_figure.png)


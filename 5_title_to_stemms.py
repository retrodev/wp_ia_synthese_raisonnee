#!/usr/bin/python
# -*- coding: utf-8 -*-
import nltk, re, csv, string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import FrenchStemmer
from nltk.stem.snowball import EnglishStemmer
from nltk.stem.snowball import GermanStemmer

# nltk.download('stopwords') # comment out if already downloaded
# nltk.download('punkt')     # comment out if already downloaded
# nltk.download('wordnet')   # comment out if already downloaded

stemmer_fr = FrenchStemmer()
stemmer_en = EnglishStemmer()
stemmer_de = GermanStemmer()

# loading the tab-separated file with all reference titles
with open('1_all_titles_with_metadata.tsv') as input_file :
    with open('6_all_titles_stemmed_with_metadata.tsv', 'w') as output_file :
        csvreader = csv.reader(input_file, delimiter='\t')
        csvwriter = csv.writer(output_file, delimiter='\t')
        # we skip the first (headers) line
        next(csvreader)
        # for each line/reference in the file we take the title
        for ref in csvreader :
            title_text = ref[6]
            # we then map languages
            if ref[5] == "fr" :
                title_lang = "french"
            elif ref[5] == "en" :
                title_lang = "english"
            elif ref[5] == "de" :
                title_lang = "german"
                
            # here we start tranformations 

            # first we convert to lowercase
            title_lower = title_text.lower()
            # then we replace all punctuation with spaces
            title_spaced = ''
            for char in title_lower :
                if char in string.punctuation :
                    title_spaced += ' '
                else :
                    title_spaced += char
            # we cut the title in tokens
            tokens = word_tokenize(title_spaced, language=title_lang)
            # then we stemm each token using the appropriate language
            tokens_stopped_stemmed = []
            for token in tokens :
                if token not in stopwords.words(title_lang) :
                    if title_lang == 'french' :
                        token_stemmed = stemmer_fr.stem(token)
                    elif title_lang == 'english' :
                        token_stemmed = stemmer_en.stem(token)
                    elif title_lang == 'german' :
                        token_stemmed = stemmer_de.stem(token)
                    tokens_stopped_stemmed.append(token_stemmed)

            # we export another tab-separated list with the same structure
            # but with the list of stemmed tokens at the end of each line
            csvwriter.writerow([*ref, tokens_stopped_stemmed])



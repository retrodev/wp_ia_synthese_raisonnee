#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv, math

# we initialize sets of counters for each dimensions we analyse
nb_by_article = {}
nb_by_article_ref_lang = {}
nb_by_article_ref_type = {}
nb_by_article_ref_decade = {}

list_langs = []
list_types = []
list_decades = []

nb_by_article_ref_type_decade = {}


# loading the tab-separated file with all reference titles and metadata
with open('1_all_titles_with_metadata.tsv') as input_file :
	csvreader = csv.reader(input_file, delimiter='\t')
	# we skip the first (headers) line
	next(csvreader)
	# for each line/reference in the file
	for ref in csvreader :
		# first we consider the article
		article = ref[0]
		if article not in nb_by_article :
			nb_by_article[article] = 1
		else :
			nb_by_article[article] += 1
		
		# we also add article references in all other counters
		if article not in nb_by_article_ref_lang :
			nb_by_article_ref_lang[article] = {}
		if article not in nb_by_article_ref_type :
			nb_by_article_ref_type[article] = {}
		if article not in nb_by_article_ref_decade :
			nb_by_article_ref_decade[article] = {}
		if article not in nb_by_article_ref_type_decade :
			nb_by_article_ref_type_decade[article] = {}
		
		# then, we look at the the ref language
		ref_lang = ref[5]
		if ref_lang not in nb_by_article_ref_lang[article] :
			nb_by_article_ref_lang[article][ref_lang] = 1
		else :
			nb_by_article_ref_lang[article][ref_lang] += 1
		if ref_lang not in list_langs :
			list_langs.append(ref_lang)
		
		# then, we look at the the ref type
		ref_type = ref[4]
		if ref_type not in nb_by_article_ref_type[article] :
			nb_by_article_ref_type[article][ref_type] = 1
		else :
			nb_by_article_ref_type[article][ref_type] += 1
		if ref_type not in list_types :
			list_types.append(ref_type)
		
		# then, we look at the the ref decade
		# to do so, we round each date to the lower decade
		if len(ref[3]) == 0 or str(ref[3]) ==  '0'  :
			decade = 'none'
		else :
			decade = math.floor(int(ref[3]) / 10) * 10
		
		if decade not in nb_by_article_ref_decade[article] :
			nb_by_article_ref_decade[article][decade] = 1
		else :
			nb_by_article_ref_decade[article][decade] += 1
		if decade not in list_decades :
			list_decades.append(decade)
		
		# then, we cross the ref type and decade
		if ref_type not in nb_by_article_ref_type_decade[article] :
			nb_by_article_ref_type_decade[article][ref_type] = {}
		if decade not in nb_by_article_ref_type_decade[article][ref_type] :
			nb_by_article_ref_type_decade[article][ref_type][decade] = 1
		else :
			nb_by_article_ref_type_decade[article][ref_type][decade] += 1

out = 'article\t'
for article in nb_by_article :
	out += article + '\t'
out += '\nréférences\t'
for article in nb_by_article :
	out += str(nb_by_article[article]) + '\t'
out += '\n'
for ref_lang in list_langs :
	out += ref_lang + '\t'
	for article in nb_by_article :
		if ref_lang in nb_by_article_ref_lang[article] :
			out += str(nb_by_article_ref_lang[article][ref_lang]) + '\t'
		else :
			out += '0\t'
	out += '\n'
for ref_type in list_types :
	out += ref_type + '\t'
	for article in nb_by_article :
		if ref_type in nb_by_article_ref_type[article] :
			out += str(nb_by_article_ref_type[article][ref_type]) + '\t'
		else :
			out += '0\t'
	out += '\n'
for decade in list_decades :
	out += str(decade) + '\t'
	for article in nb_by_article :
		if decade in nb_by_article_ref_decade[article] :
			out += str(nb_by_article_ref_decade[article][decade]) + '\t'
		else :
			out += '0\t'
	out += '\n'

fichier = open('3_ref_stats.tsv', 'w')
fichier.write(out)
fichier.close()



	


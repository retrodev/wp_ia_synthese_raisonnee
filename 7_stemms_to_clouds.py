#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv, math
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from PIL import Image

# we will use dictionnaries to sort stemmed title in bags
# there will be topic bags (AI, DL, ML), lang bags (en, fr), ...
titles_by_article_topic = {}
stemms_by_article_topic = {}
titles_by_article_lang = {}
stemms_by_article_lang = {}
titles_by_ref_period = {}
stemms_by_ref_period = {}
titles_by_ref_type = {}
stemms_by_ref_type = {}

titles_by_article_and_type = {}
stemms_by_article_and_type = {}


with open('6_all_titles_stemmed_with_metadata.tsv') as input_file :
    csvreader = csv.reader(input_file, delimiter='\t')
    # we skip the first/headers line
    next(csvreader)
    # for each line/ref in the file
    for ref in csvreader :
        # we turn the list of stemmed words into a python list
        words = ref[7].strip('[]').replace('\'', '').replace(' ', '').split(',')

        # first we consider the article's topic (AI, ML, DL)
        # if the topic is not an existing bag, we create the empty bag
        if ref[1] not in titles_by_article_topic :
            titles_by_article_topic[ref[1]] = ''
        # we put the title's stemmed words in the corresponding topic bag
        for stemm in words :
            # we filter out stemm of 1 char
            if len(stemm) > 1 :
                # each stemm is prefixed with the ref lang
                # (wich may be different than the article lang, when the fr article use en refs)
                # titles_by_article_topic[ref[1]] += ref[5] + '_' + stemm + ' '
                # nope, changed, we no longer use lang prefixes
                titles_by_article_topic[ref[1]] += stemm + ' '
        
        # then we consider the article's lang (en, fr)
        if ref[2] not in titles_by_article_lang :
            titles_by_article_lang[ref[2]] = ''
            # we put the title's stemmed words in the corresponding lang bag
        for stemm in words :
            # we filter out stemm of 1 char
            if len(stemm) > 1 :
                # each stemm is prefixed with the ref lang
                # titles_by_article_lang[ref[2]] += ref[5] + '_' + stemm + ' '
                # nope, changed, we no longer use lang prefixes
                titles_by_article_lang[ref[2]] += stemm + ' '
        
        # then we consider the reference's date
        # this time, we do not consider the raw date, but only decades
        # to do so, we round each date to the lower decade
        if len(ref[3]) == 0 or str(ref[3]) ==  '0'  :
        	decade = 'none'
        else :
	        decade = math.floor(int(ref[3]) / 10) * 10
        if decade not in titles_by_ref_period :
            titles_by_ref_period[decade] = ''
        # we put the title's stemmed words in the corresponding type bag
        for stemm in words :
            # we filter out stemm of 1 char
            if len(stemm) > 1 :
                # each stemm is prefixed with the ref lang
                # titles_by_ref_period[decade] += ref[5] + '_' + stemm + ' '
                # nope, changed, we no longer use lang prefixes
                titles_by_ref_period[decade] += stemm + ' '
                
        # to deal with subcorpuses
        if ref[0] not in titles_by_article_and_type :
            titles_by_article_and_type[ref[0]] = {}
            stemms_by_article_and_type[ref[0]] = {}
        if ref[4] not in titles_by_article_and_type[ref[0]] :
            titles_by_article_and_type[ref[0]][ref[4]] = ''
            stemms_by_article_and_type[ref[0]][ref[4]] = []
        
        # then we consider the reference's type (livre, science ...)
        if ref[4] not in titles_by_ref_type :
            titles_by_ref_type[ref[4]] = ''
        # we put the title's stemmed words in the corresponding type bag
        for stemm in words :
            # we filter out stemm of 1 char
            if len(stemm) > 1 :
                # each stemm is prefixed with the ref lang
                # titles_by_ref_type[ref[4]] += ref[5] + '_' + stemm + ' '
                # nope, changed, we no longer use lang prefixes
                titles_by_ref_type[ref[4]] += stemm + ' '
                titles_by_article_and_type[ref[0]][ref[4]] += stemm + ' '
                stemms_by_article_and_type[ref[0]][ref[4]].append(stemm)

# now, we generate the wordclouds for each bag of each kind

for topic in titles_by_article_topic :
    fig = plt.figure(figsize=(6, 3), dpi=150)
    wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_topic[topic])
    plt.imshow(wordcloud)
    plt.axis("off")
    fig.savefig('cloud_topic_' + topic + '.png')
    # we also create lists of the 20 most frequent terms
    stemm_list = titles_by_article_topic[topic].split(' ')
    stemm_count = {}
    for stemm in stemm_list :
    	if stemm not in stemm_count :
    		stemm_count[stemm] = 1
    	else :
    		stemm_count[stemm] += 1
    stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
    fichier = open('top_topic_' + topic + '.csv', 'w')
    for stemm in stemm_sorted[:20] :
    	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
    fichier.close()
    

for lang in titles_by_article_lang :
    fig = plt.figure(figsize=(6, 3), dpi=150)
    wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_lang[lang])
    plt.imshow(wordcloud)
    plt.axis("off")
    fig.savefig('cloud_lang_' + lang + '.png')
    # we also create lists of the 20 most frequent terms
    stemm_list = titles_by_article_lang[lang].split(' ')
    stemm_count = {}
    for stemm in stemm_list :
    	if stemm not in stemm_count :
    		stemm_count[stemm] = 1
    	else :
    		stemm_count[stemm] += 1
    stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
    fichier = open('top_lang_' + lang + '.csv', 'w')
    for stemm in stemm_sorted[:20] :
    	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
    fichier.close()

for decade in titles_by_ref_period :
    fig = plt.figure(figsize=(6, 3), dpi=150)
    wordcloud = WordCloud(background_color = 'white').generate(titles_by_ref_period[decade])
    plt.imshow(wordcloud)
    plt.axis("off")
    fig.savefig('cloud_period_' + str(decade) + '.png')
    # we also create lists of the 20 most frequent terms
    stemm_list = titles_by_ref_period[decade].split(' ')
    stemm_count = {}
    for stemm in stemm_list :
    	if stemm not in stemm_count :
    		stemm_count[stemm] = 1
    	else :
    		stemm_count[stemm] += 1
    stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
    fichier = open('top_decade_' + str(decade) + '.csv', 'w')
    for stemm in stemm_sorted[:20] :
    	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
    fichier.close()

for reftype in titles_by_ref_type :
    fig = plt.figure(figsize=(6, 3), dpi=150)
    wordcloud = WordCloud(background_color = 'white').generate(titles_by_ref_type[reftype])
    plt.imshow(wordcloud)
    plt.axis("off")
    fig.savefig('cloud_type_' + reftype + '.png')
    # we also create lists of the 20 most frequent terms
    stemm_list = titles_by_ref_type[reftype].split(' ')
    stemm_count = {}
    for stemm in stemm_list :
    	if stemm not in stemm_count :
    		stemm_count[stemm] = 1
    	else :
    		stemm_count[stemm] += 1
    stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
    fichier = open('top_type_' + reftype + '.csv', 'w')
    for stemm in stemm_sorted[:20] :
    	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
    fichier.close()

# manual exports for a few subcorpuses

fig = plt.figure(figsize=(6, 3), dpi=150)
wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_and_type['en_Artificial_Intelligence']['presse'])
plt.imshow(wordcloud)
plt.axis("off")
fig.savefig('cloud_titles_AI_en_presse.png')

stemm_count = {}
for stemm in stemms_by_article_and_type['en_Artificial_Intelligence']['presse'] :
	if stemm not in stemm_count :
		stemm_count[stemm] = 1
	else :
		stemm_count[stemm] += 1
stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
fichier = open('top_titles_AI_en_presse.csv', 'w')
for stemm in stemm_sorted[:20] :
	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
fichier.close()


fig = plt.figure(figsize=(6, 3), dpi=150)
wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_and_type['en_Artificial_Intelligence']['science'])
plt.imshow(wordcloud)
plt.axis("off")
fig.savefig('cloud_titles_AI_en_science.png')

stemm_count = {}
for stemm in stemms_by_article_and_type['en_Artificial_Intelligence']['science'] :
	if stemm not in stemm_count :
		stemm_count[stemm] = 1
	else :
		stemm_count[stemm] += 1
stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
fichier = open('top_titles_AI_en_science.csv', 'w')
for stemm in stemm_sorted[:20] :
	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
fichier.close()

fig = plt.figure(figsize=(6, 3), dpi=150)
wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_and_type['fr_Intelligence_artificielle']['presse'])
plt.imshow(wordcloud)
plt.axis("off")
fig.savefig('cloud_titles_AI_fr_presse.png')

stemm_count = {}
for stemm in stemms_by_article_and_type['fr_Intelligence_artificielle']['presse'] :
	if stemm not in stemm_count :
		stemm_count[stemm] = 1
	else :
		stemm_count[stemm] += 1
stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
fichier = open('top_title_AI_fr_presse.csv', 'w')
for stemm in stemm_sorted[:20] :
	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
fichier.close()

fig = plt.figure(figsize=(6, 3), dpi=150)
wordcloud = WordCloud(background_color = 'white').generate(titles_by_article_and_type['fr_Intelligence_artificielle']['science'])
plt.imshow(wordcloud)
plt.axis("off")
fig.savefig('cloud_titles_AI_fr_science.png')

stemm_count = {}
for stemm in stemms_by_article_and_type['fr_Intelligence_artificielle']['science'] :
	if stemm not in stemm_count :
		stemm_count[stemm] = 1
	else :
		stemm_count[stemm] += 1
stemm_sorted = sorted(stemm_count.items(), key=lambda k:k[1], reverse=True)
fichier = open('top_titles_AI_fr_science.csv', 'w')
for stemm in stemm_sorted[:20] :
	fichier.write(stemm[0] + '\t' + str(stemm[1]) + '\n')
fichier.close()



